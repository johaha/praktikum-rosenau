plot OmegaScanYBCO007.xrdml.Z00
plot OmegaScanYBCO007.xrdml.Z00 u 1
plot "OmegaScanYBCO007.xrdml.Z00" u 1
f(x) = a*x**2 +b*x + c
fit f(x) 'OmegaScanYBCO007.xrdml.Z00' u 1 via a,b,c
fit f(x) 'OmegaScanYBCO007.xrdml.Z00
fit f(x) 'OmegaScanYBCO007.xrdml.Z00'
fit f(x)
fit f(x) 'OmegaScanYBCO007.xrdml.Z00' u 0:1
fit f(x) 'OmegaScanYBCO007.xrdml.Z00' u 0:1 via a,b,c
plot
# Define initial parameters
gnuplot> PI=3.14; s=1; m=1; a=1;
# Define function we want to fit
gnuplot> gauss(x) = a/(2*PI*s**2)**0.5*exp(-(x-m)**2/(2*s**2)) 
PI=3.14; s=1; m=1; a=1;
gauss(x) = a/(2*PI*s**2)**0.5*exp(-(x-m)**2/(2*s**2)) 
fit gauss(x) 'OmegaScanYBCO007.xrdml.Z00' u 0:1 via s,m,a
m= 140
s=20
fit gauss(x) 'OmegaScanYBCO007.xrdml.Z00' u 0:1 via s, m, a
gauss(x) = 1/(2*PI*s**2)**0.5*exp(-(x-m)**2/(2*s**2))
s
echo s
set s
plot s
print s
print m
print pi
print PI
gauss(x) = 1/(2*pi*s**2)**0.5*exp(-(x-m)**2/(2*s**2))
fit gauss(x) 'OmegaScanYBCO007.xrdml.Z00' u 0:1 via s, m
plot gauss(x), 'OmegaScanYBCO007.xrdml.Z00' u 0:1
set xrange[0:250]
plot gauss(x)
plot 'OmegaScanYBCO007.xrdml.Z00' u0:1
a=57699.6
s=-39.1643
m=140.174
fit g(x) 'OmegaScanYBCO007.xrdml.Z00' u 0:1 via a,s,m
a=57713
s=39.1626
set xrange[0:240]
plot g(x), 'OmegaScanYBCO007.xrdml.Z00' u 0:1
plot 'OmegaScanYBCO007.xrdml.Z00'
plot 'OmegaScanYBCO007.xrdml.Z00' u 0:1
!vi OmegaScanYBCO007.xrdml.Z00
!cat OmegaScanYBCO007.xrdml.Z00 > OmegaScanYBCO007.xrdml.Z00_data-only.csv
!vi OmegaScanYBCO007.xrdml.Z00_data-only.csv
plot g(x), 'OmegaScanYBCO007.xrdml.Z00_data-only.csv' u 0:1
g(x)=a/(s*sqrt(2*pi))*exp(-(x-m)**2/(2*s**2))
fit g(x) 'OmegaScanYBCO007.xrdml.Z00_data-only.csv' u 0:1 via a,s,m
set xrange[0:220]
plot g(x)
plot 'OmegaScanYBCO007.xrdml.Z00_data-only.csv' u 0:1
echo a,s,m
print a,s,m
a=24.713
s=0.19585
m=27.5900
fit g(x) 'OmegaScanYBCO007.xrdml.Z00_data-only.csv' u 0:1 via a,m,s
f(x)=a*x +b
fit f(x) 'winkel-cm.txt' u 1:2 via a,b
fit f(x) 'winkel-cm.txt' u 2:1 via a,b
plot f(x), 'winkel-cm.txt'
plot f(x), 'winkel-cm.txt' u 2:1
plot 'AAU01804.GAM'
!mv messdaten_20150401_nga-untergrund.csv messdaten_20150401_nga-messing.csv
f(x)=a*exp(-x/b)
plot f(x)
print f(x)
f(x9
f(x)
logscale y
set logscale y off
fit f(x) 'messdaten_20150401_nga-messing.csv' u 1:($2-8) via a,b
plot f(x), 'messdaten_20150401_nga-messing.csv'
f2(x)=a*exp(-x/b)+c*exp(-x/d)
fit f2(x) 'messdaten_20150401_nga-messing.csv' u 1:($2-8) via a,b,c,d
plot f2(x)
plot f2(x), 'messdaten_20150401_nga-messing.csv'
fit f(x) 'messdaten_20150401_nga-messing.csv
plot f(x), 'messdaten_20150401_nga-messing.csv' u 1:($2-8)
g(x)=x^(1/2)
fit g(x) 'messdaten_20150401_nga-messing.csv' u 1:($2-8)
g(x)=a*x^(1/2)
fit g(x) 'messdaten_20150401_nga-messing.csv' u 1:($2-8) via a
p�lot 'messdaten_20150401_nga-messing.csv'
plot 'messdaten_20150401_nga-messing.csv'
unset logscale y
fit f(x) 'messdaten_20150401_nga-messing.csv' via a,b
plot 'messdaten_20150401_nga-messing.csv', f(x)
g(x)=a*x
fit g(x) 'messdaten_20150401_nga-messing.csv' via a
plot g(x), messdaten_20150401_nga-messing.csv
fit g(x) 'messdaten_20150401_nga-messing.csv' u 1:(ln($2)) via a
fit g(x) 'messdaten_20150401_nga-messing.csv' u 1:(log($2)) via a
plot 'messdaten_20150401_nga-messing.csv' u 1:(log($2))
plot g(x), 'messdaten_20150401_nga-messing.csv'
fit g(x) 'messdaten_20150401_nga-messing.csv' u 1:(log($2)) via a,b
set xrange[0,4]
set xrange[0:4]
set yrange[0:4]
set yrange[0:5]
plot g(x), 'messdaten_20150401_nga-messing.csv' u 1:(log($2))
print b
print a
g(x)=-a*x +b
g(x)=-x/a +b
g(x)=-x/(a*60*60*24) +b
g(x)=-x/(a) +b
!vi 'messdaten_20150401_nga-messing.csv'
plot 'messdaten_20150401_nga-messing.csv' u 1:($2-6)
!vi messdaten_20150401_nga-messing.csv
plot g(x), 'messdaten_20150401_nga-messing.csv' u 1:log($2-6)
set yrange[0:8]
g(x)=a*x+b
fit g(x) 'messdaten_20150401_nga-messing.csv' u 1:(log($2-6)) via a,b
plot g(x), 'messdaten_20150401_nga-messing.csv' u 1:(log($2-6))
!sensor
!sensors
plot g(x), 'messdaten_20150401_nga-messing.csv' u 1:(log($2-6)):(sqrt($2-6)) with yerrorbars
plot 'messdaten_20150401_nga-messing.csv' u 1:($2-6):(sqrt($2-6)) with yerrorbars
exit
plot for [IDX=0:4] 'messdaten_20150401_nga.csv' i IDX u 1:3:4 w lines,yerrorbars title columnheader(1)
plot for [IDX=0:4] 'messdaten_20150401_nga.csv' i IDX u 1:3:4 w yerrorbars title columnheader(1)
plot for [IDX=0:4] 'messdaten_20150401_nga.csv' i IDX u 1:3:5 w yerrorbars title columnheader(1)
plot for [IDX=0:4] 'messdaten_20150401_nga.csv' i IDX u 1:3:5 w yerrorbars w l  title columnheader(1)
plot for [IDX=0:4] 'messdaten_20150401_nga.csv' i IDX u 1:3:5 w yerrorbars l  title columnheader(1)
plot for [IDX=0:4] 'messdaten_20150401_nga.csv' i IDX u 1:3:5 w yerrorbars lines  title columnheader(1)
plot for [IDX=0:4] 'messdaten_20150401_nga.csv' i IDX u 1:3:5 w lines, yerrorbars title columnheader(1)
plot for [IDX=0:4] 'messdaten_20150401_nga.csv' i IDX u 1:3:5 with lines, yerrorbars title columnheader(1)
plot for [IDX=0:4] 'messdaten_20150401_nga.csv' i IDX u 1:3:5 w yerrorbars title columnheader(1) with lines
plot for [IDX=0:4] 'messdaten_20150401_nga.csv' i IDX u 1:3:5 w yerrorbars ls1 title columnheader(1)
plot for [IDX=0:4] 'messdaten_20150401_nga.csv' i IDX u 1:3:5 w yerrorbars ls 2 title columnheader(1)
plot for [IDX=0:4] 'messdaten_20150401_nga.csv' i IDX u 1:3:5 w yerrorbars l title columnheader(1)
plot for [IDX=0:4] 'messdaten_20150401_nga.csv' i IDX u 1:3:5 w yerrorbars ls 1 title columnheader(1)
plot for [IDX=0:4] 'messdaten_20150401_nga.csv' i IDX u 1:3:5 w yerrorbars lp 1 title columnheader(1)
plot for [IDX=0:4] 'messdaten_20150401_nga.csv' i IDX u 1:3:5 w l yerrorbars title columnheader(1)
set autoscale
plot for [IDX=0:4] 'messdaten_20150401_nga.csv' i IDX u 1:3:5 with yerrorbars title columnheader(1)
plot for [IDX=0:4] 'messdaten_20150401_nga.csv' i IDX u 1:3:5 with lt 1yerrorbars title columnheader(1)
plot for [IDX=0:4] 'messdaten_20150401_nga.csv' i IDX u 1:3:5 with lt 1 yerrorbars title columnheader(1)
plot for [IDX=0:4] 'messdaten_20150401_nga.csv' i IDX u 1:3:5 with errorbars lt 1 title columnheader(1)
help errorbars
plot for [IDX=0:4] 'messdaten_20150401_nga.csv' i IDX u 1:3:5 with errorbars pt 1 title columnheader(1),
plot for [IDX=0:4] 'messdaten_20150401_nga.csv' i IDX u 1:3:5 w l title columnheader(1)
plot for [IDX=0:4] 'messdaten_20150401_nga.csv' i IDX u 1:3:5 with errorbars pt 1 title columnheader(1),\
for [IDX=0:4] 'messdaten_20150401_nga.csv' i IDX u 1:3:5 w l title columnheader(1)
plot for [IDX=0:4] 'messdaten_20150401_nga.csv' i IDX u 1:3:5 with errorbars pt 1 title columnheader(1)
plot for [IDX=0:4] 'messdaten_20150401_nga.csv' i IDX u 1:3:5 with errorbars pt 2 title columnheader(1)
plot for [IDX=0:4] 'messdaten_20150401_nga.csv' i IDX u 1:3:5 with errorbars pt 3 title columnheader(1)
set logscale
!vi messdaten_20150401_nga.csv
unset logscale
set logscale y
set term tikz color solid
set output 'na_aktivitaeten_all.tikz'
!ls
!ls -lFa
quit
set term tikz
set output 'nga_aktivitaeten_all.tikz'
replot
plot for [IDX=0:4] 'messdaten_20150401_nga.csv' i IDX u 1:3:5 with errorbars title columnheader(1)
q
hi !hi
hi
hi "gnuplot_fitting.gp"
