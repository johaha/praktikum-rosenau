;------- Select Crate -------------------------------
CAMAC >Select crate
1
;------- Edit Paramters of read spectra -------------------
CAMAC >Edit
0, 0, 0, 0, 40, 0, 0, 150, 55, 0, 0, 0, 0, 0, 0, 0, 1.0, 1,pau
;------- Setup CAMAC-Slots -------------------------------
CAMAC >Configuration
8,10,0,0,0,0,0,0,23
;------- Setup CAMAC-ADCs -------------------------------
CAMAC >Setup ADCs
y,y,y,y,500,500,500,500,300,500,500,300,50,500,500,500,500,500,500,500,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
;------- Setup CAMAC-HMs -------------------------------
CAMAC >Setup HMs
y,y,y,y,n,n,n,n,n,n,n,n,n,n,n,n
;------- Setup Scalers -------------------------------
CAMAC >Setup Scalers
1, 3
;------- Setup Spektren ----------------------------------
Setup >Spectra
8192,8192
; ------- Setup Clipboard ---------------------------------
Setup >Clipboard
c:\clp\,6
; ------- Setup Screensaver ---------------------------------
Setup >Screensaver
n,0,5,30
; ------- Setup Integral ---------------------------------
Setup >Integral
praint.txt
; ------- Clipboard an -------------------------------------
Spectrum >All Clipboard
-
; ------- Show Fits ---------------------------------
Fitting >Show
; ------- Show Integrals ---------------------------------
Integral >Show
; ------- Show Calibrations ---------------------------------
Calibrate >Show
; ------- Initialize PC-Interface ----------------------------
CAMAC >Initialize interface
-
;CAMAC >Initialize crate
;-
;CAMAC >Start
;-
;CAMAC >Read
;-
