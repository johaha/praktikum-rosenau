>>                    RELATIVISTIC KINEMATICS PROGRAM
                        23NA(P,P)23NA                 
            22.989768(   1.007276 ,   1.007276 )  22.989768
0DE/DX1= 150.1 KEV/MG/CM2     QVAL(GS)=    .000 +-    .002 MEV     EBEAM=   1.373 MEV
  EXC ENERGY =   .0000  ********************   THRESHOLD ENERGY=   .0000
0 THETA3    THETA     KIN EN3  LAB TO CM  THETA4   KIN EN4  DE/DTHETA3  DE/DX3      T.O.F.
  LAB DEG   CM DEG     (MEV)    CONVF3   LAB DEG     (MEV)   KEV/DEG  KEV/MG/CM2     NS/M 
    45.0    46.8       1.338      .941      66.6      .035       1.4     152.7      62.523
>>                    RELATIVISTIC KINEMATICS PROGRAM
                        12C(P,P)12C                   
            12.000000(   1.007276 ,   1.007276 )  12.000000
0DE/DX1= 187.4 KEV/MG/CM2     QVAL(GS)=    .000 +-    .000 MEV     EBEAM=   1.373 MEV
  EXC ENERGY =   .0000  ********************   THRESHOLD ENERGY=   .0000
0 THETA3    THETA     KIN EN3  LAB TO CM  THETA4   KIN EN4  DE/DTHETA3  DE/DX3      T.O.F.
  LAB DEG   CM DEG     (MEV)    CONVF3   LAB DEG     (MEV)   KEV/DEG  KEV/MG/CM2     NS/M 
    45.0    48.4       1.307      .892      65.8      .066       2.7     194.1      63.262
>>                    RELATIVISTIC KINEMATICS PROGRAM
                        13C(P,P)13C                   
            13.003355(   1.007276 ,   1.007276 )  13.003355
0DE/DX1= 172.9 KEV/MG/CM2     QVAL(GS)=    .000 +-    .000 MEV     EBEAM=   1.373 MEV
  EXC ENERGY =   .0000  ********************   THRESHOLD ENERGY=   .0000
0 THETA3    THETA     KIN EN3  LAB TO CM  THETA4   KIN EN4  DE/DTHETA3  DE/DX3      T.O.F.
  LAB DEG   CM DEG     (MEV)    CONVF3   LAB DEG     (MEV)   KEV/DEG  KEV/MG/CM2     NS/M 
    45.0    48.1       1.312      .900      65.9      .061       2.5     178.6      63.142
>>                    RELATIVISTIC KINEMATICS PROGRAM
                        197AU(P,P)197AU               
           196.966543(   1.007276 ,   1.007276 ) 196.966543
0DE/DX1=  52.9 KEV/MG/CM2     QVAL(GS)=    .000 +-    .008 MEV     EBEAM=   1.373 MEV
  EXC ENERGY =   .0000  ********************   THRESHOLD ENERGY=   .0000
0 THETA3    THETA     KIN EN3  LAB TO CM  THETA4   KIN EN4  DE/DTHETA3  DE/DX3      T.O.F.
  LAB DEG   CM DEG     (MEV)    CONVF3   LAB DEG     (MEV)   KEV/DEG  KEV/MG/CM2     NS/M 
    45.0    45.2       1.369      .993      67.4      .004        .2      53.0      61.819
>>                    RELATIVISTIC KINEMATICS PROGRAM
                        19F(P,P)19F                   
            18.998403(   1.007276 ,   1.007276 )  18.998403
0DE/DX1= 158.1 KEV/MG/CM2     QVAL(GS)=    .000 +-    .001 MEV     EBEAM=   1.373 MEV
  EXC ENERGY =   .0000  ********************   THRESHOLD ENERGY=   .0000
0 THETA3    THETA     KIN EN3  LAB TO CM  THETA4   KIN EN4  DE/DTHETA3  DE/DX3      T.O.F.
  LAB DEG   CM DEG     (MEV)    CONVF3   LAB DEG     (MEV)   KEV/DEG  KEV/MG/CM2     NS/M 
    45.0    47.2       1.331      .930      66.4      .042       1.7     161.5      62.691
>>                   RELATIVISTIC KINEMATICS PROGRAM
                       19F(P,4HE)16O                 
            18.998403(   1.007276 ,   4.001506 )  15.995463
0DE/DX1= 158.1 KEV/MG/CM2     QVAL(GS)=   8.114 +-    .001 MEV     EBEAM=   1.373 MEV
  EXC ENERGY =  4.7310  ********************   THRESHOLD ENERGY= -3.5618
0 THETA3    THETA     KIN EN3  LAB TO CM  THETA4   KIN EN4  DE/DTHETA3  DE/DX3      T.O.F.
  LAB DEG   CM DEG     (MEV)    CONVF3   LAB DEG     (MEV)   KEV/DEG  KEV/MG/CM2     NS/M 
    45.0    47.5       4.070      .920     120.5      .685       6.1     768.3      71.433
  EXC ENERGY =  6.0490  ********************   THRESHOLD ENERGY= -2.1741
0 THETA3    THETA     KIN EN3  LAB TO CM  THETA4   KIN EN4  DE/DTHETA3  DE/DX3      T.O.F.
  LAB DEG   CM DEG     (MEV)    CONVF3   LAB DEG     (MEV)   KEV/DEG  KEV/MG/CM2     NS/M 
    45.0    47.9       2.967      .907     117.4      .470       5.3     935.4      83.646
  EXC ENERGY =  6.1300  ********************   THRESHOLD ENERGY= -2.0888
0 THETA3    THETA     KIN EN3  LAB TO CM  THETA4   KIN EN4  DE/DTHETA3  DE/DX3      T.O.F.
  LAB DEG   CM DEG     (MEV)    CONVF3   LAB DEG     (MEV)   KEV/DEG  KEV/MG/CM2     NS/M 
    45.0    47.9       2.899      .906     117.1      .458       5.2     948.4      84.622
  EXC ENERGY =  6.9170  ********************   THRESHOLD ENERGY= -1.2601
0 THETA3    THETA     KIN EN3  LAB TO CM  THETA4   KIN EN4  DE/DTHETA3  DE/DX3      T.O.F.
  LAB DEG   CM DEG     (MEV)    CONVF3   LAB DEG     (MEV)   KEV/DEG  KEV/MG/CM2     NS/M 
    45.0    48.4       2.235      .893     113.9      .335       4.6    1098.3      96.363
  EXC ENERGY =  7.1170  ********************   THRESHOLD ENERGY= -1.0496
0 THETA3    THETA     KIN EN3  LAB TO CM  THETA4   KIN EN4  DE/DTHETA3  DE/DX3      T.O.F.
  LAB DEG   CM DEG     (MEV)    CONVF3   LAB DEG     (MEV)   KEV/DEG  KEV/MG/CM2     NS/M 
    45.0    48.5       2.066      .889     112.8      .304       4.4    1144.7     100.237
