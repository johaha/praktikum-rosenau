# -*- coding: utf-8 -*-
import os  # filesystem operations
import fnmatch as fn  # pattern matching
import re  # regex
import numpy as np


def find_files(matchtext, path='.'):
    """Create a list of filenames matching matchtext.extention

    returns: list of filenames """
    return fn.filter(os.listdir(path), matchtext)


def load_data(extention, path='.'):
    """Create a dictionary witch contains all datasets found in path
    filenames are trimed by extention"""
    files = find_files('*.' + extention, path)
    return {re.findall(r'(.*)\.' + extention, f)[0]: np.loadtxt(f).T
            for f in files}


