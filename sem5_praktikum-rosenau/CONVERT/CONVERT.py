# -*- coding: utf-8 -*-
import re
import struct
import filetools
import numpy as np
import pylab as pl


# class McMain:
#    """ Data writen by McMain in a .DAT file"""
#    head  # 8 byte unknown content
#    histo  # long[0:8191] chanels of histogramm
#    angle  # float (8 bytes)
#    run  # int (2 bytes)
#    spec_name  # char
#    scalar_1  # long (8 bytes)
#    scalar_2  # long (8 bytes)
#    energy  # float (8 bytes)
#    def __init__(self, head, histo, angle, run, spec_name, scalar_1, scalar_2,
#                 energy):
#        self.head = head
#        self.histo = histo
#        self.angle = angle
#        self.run = run
#        self.spec_name = spec_name
#        self.scalar_1 = scalar_1
#        self.scalar_2 = scalar_2
#        self.energy = energy

def load_binary(extention, path='.'):
    """Create a dictionary witch contains all datasets found in
    filenames are trimed by extention"""
    result = {}
    files = filetools.find_files('*.' + extention, path)
    for fi in files:
        with open(fi, 'rb') as f:
            data = []
            f.read(5)
            b = f.read(4)
            while len(b) == 4:
                data.append(struct.unpack(">L", b)[0])
                b = f.read(4)
            result[re.findall(r'(.*)\.' + extention, fi)[0]] = data
    return result


def load_McMain(extention, path='.'):
    """Try to load a file created by McMain"""
    result = {}
    files = filetools.find_files('*.' + extention, path)
    for fi in files:
        with open(fi, 'rb') as f:
            mcmain = {}
            # 8 Byte unknown content
            mcmain['head'] = f.read(5)
            data = []
            i = 0
            while i < 8192:
                i += 1
                data.append(struct.unpack(">i", f.read(4))[0])
            mcmain['histo'] = data
            mcmain['angle'] = struct.unpack(">d", f.read(8))[0]
            mcmain['run'] = struct.unpack(">h", f.read(2))[0]
            mcmain['spec_name'] = f.read(1)
            mcmain['scalar_1'] = struct.unpack(">q", f.read(8))[0]
            # mcmain['scalar_2'] = struct.unpack(">q", f.read(8))[0]
            # mcmain['energy'] = struct.unpack(">d", f.read(8))[0]
            result[re.findall(r'(.*)\.' + extention, fi)[0]] = mcmain
    return result


def _save_data(data):
    for name in data:
        with open(name + '.txt', 'w') as f:
            for i in data[name]:
                f.write(str(i) + '\n')


def save_McMain_histo(data):
    for name in data:
        with open(name + '.txt', 'w') as f:
            for i in data[name]['histo']:
                f.write(str(i) + '\n')


if __name__ == '__main__':
    data = load_McMain('DAT')
#    print(data)
#    d = data['PAU00102']
#    print(d)
#    pl.close('all')
#    pl.plot(np.arange(len(d['histo'])), d['histo'], '-')
#    pl.show()
#    pl.close('all')
    save_McMain_histo(data)
